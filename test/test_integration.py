import unittest

import yaml

from official_build import build


MANIFEST_FIXTURE = """
repositoryPath: 'atlassian/ssh-run'
version: '0.2.6'
"""


class BuildE2ERealTest(unittest.TestCase):
    def test_real_check_new_structure(self):

        context = build.extract_information(yaml.safe_load(MANIFEST_FIXTURE), 'pipes/ssh-run.yml')
        failures = build.validate(context)
        self.assertEqual(len(failures.criticals), 0)
        self.assertEqual(len(failures.warnings), 1)
        self.assertRegexpMatches(failures.warnings[0], r'\d+.\d+.\d+ version available. Current version is 0.2.6')
